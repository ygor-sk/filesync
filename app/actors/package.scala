import akka.actor.Props
import play.api.libs.concurrent.Akka
import services.DashboardService.WatchedFile
import play.api.Play.current

package object actors {

  val fileWatcherActor = Akka.system.actorOf(Props[FileWatcherMasterActor])

  case class OnWebSocketConnection(webSocket: WebSocketActor)

  case class OnWebSocketDisconnection(webSocketActor: WebSocketActor)

  case object ReloadConfiguration

  case class RenderFileStatus(watchedFile: WatchedFile,
                              sourceFileStatus: String,
                              targetFileStatus: String)

  case class RenderFileSyncStatus(watchedFile: WatchedFile,
                                  syncedStatus: String,
                                  color: String)

  case object Recheck

  case object StartSyncing

  case object StopSyncing


}
