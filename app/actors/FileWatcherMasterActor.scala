package actors

import akka.actor._
import akka.event.LoggingReceive
import play.api.libs.json.{JsBoolean, JsObject, JsString}
import services.DashboardService
import services.DashboardService.WatchedFile


class FileWatcherMasterActor extends Actor {

  var webSocketActors: Set[WebSocketActor] = Set()
  var workerActors: Map[WatchedFile, ActorRef] = Map()

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    self ! ReloadConfiguration
  }

  override def receive: Receive = LoggingReceive {
    case OnWebSocketConnection(webSocketActor) =>
      println("New browser websocket connected")
      webSocketActors = webSocketActors + webSocketActor
      workerActors.values.foreach(worker => worker ! Recheck)
    case OnWebSocketDisconnection(webSocketActor) =>
      println("Browser websocket disconnected")
      webSocketActors = webSocketActors - webSocketActor;
    case ReloadConfiguration =>
      println("Reloading configuration")

      sendToAllWebSockets(JsObject(Seq(
        ("reloadConfiguration", JsBoolean(true))
      )))


      val watchedFiles = DashboardService.getWatchedFiles(DashboardService.readConfigurationFile)

      val existingFiles = watchedFiles.toSet.intersect(workerActors.keys.toSet)
      val newFiles = watchedFiles.toSet -- workerActors.keys
      val removedFiles = workerActors.keys.toSet -- watchedFiles

      newFiles.foreach { file =>
        println(s"Starting monitoring of: $file")
        val newWorker = context.actorOf(Props(new FileWatcherWorkerActor(self, file)))
        workerActors = workerActors + (file -> newWorker)
        newWorker ! StartSyncing
      }
      existingFiles.foreach { file =>
        println(s"Resuming monitoring of: $file")
        workerActors(file) ! StartSyncing
      }
      removedFiles.foreach { file =>
        println(s"Stopping monitoring of: $file")
        workerActors(file) ! StopSyncing
      }

    case RenderFileStatus(watchedFile, sourceFileStatus, targetFileStatus) =>
      println(s"Updating status of: $watchedFile. Source: $sourceFileStatus, target: $targetFileStatus")
      sendToAllWebSockets(JsObject(Seq(
        ("file", JsString(watchedFile.uniqueId)),
        ("sourceFileStatus", JsString(sourceFileStatus)),
        ("targetFileStatus", JsString(targetFileStatus))
      )))
    case RenderFileSyncStatus(watchedFile, syncedStatus, color) =>
      println(s"Updating sync status of: $watchedFile. Status: $syncedStatus")
      sendToAllWebSockets(JsObject(Seq(
        ("file", JsString(watchedFile.uniqueId)),
        ("syncedStatus", JsString(syncedStatus)),
        ("color", JsString(color))
      )))
  }

  def sendToAllWebSockets(message: Any): Unit = {
    webSocketActors.foreach(actor => actor.webSocketOutput ! message)
  }
}
