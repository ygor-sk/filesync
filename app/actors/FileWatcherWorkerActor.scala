package actors

import java.io.File
import java.nio.file.StandardWatchEventKinds._
import java.nio.file.{Files, Paths, StandardCopyOption}
import java.util.Date
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef}
import akka.event.LoggingReceive
import com.beachape.filemanagement.Messages.{RegisterCallback, UnRegisterCallback}
import com.beachape.filemanagement.MonitorActor
import org.apache.commons.codec.digest.DigestUtils
import play.api.Play.current
import play.api.libs.concurrent.Akka
import services.DashboardService.WatchedFile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

class FileWatcherWorkerActor(masterActor: ActorRef, watchedFile: WatchedFile) extends Actor {

  val timestampFormat = new java.text.SimpleDateFormat("dd-MM-yyyy hh:mm:ss")

  val fileMonitorActor =  context.actorOf(MonitorActor(concurrency = 2))

  var isActive = true
  var lastSynced: Option[Date] = None
  var lastSyncAttempt: Option[Date] = None

  case class ExistingFile(size: Long, hash: String)

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    println("Creating worker")
    Seq(ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY).foreach(kind =>
      fileMonitorActor ! RegisterCallback(
        kind,
        None,
        recursive = false,
        path = Paths.get(watchedFile.sourceFileName).getParent,
        (path) => if (path == Paths.get(watchedFile.sourceFileName)) self ! Recheck
      )
    )
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    Seq(ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY).foreach(kind =>
      fileMonitorActor ! UnRegisterCallback(kind, recursive = false, Paths.get(watchedFile.sourceFileName).getParent)
    )
  }

  override def receive: Receive = LoggingReceive {
    case StartSyncing =>
      isActive = true
      self ! Recheck
    case StopSyncing =>
      isActive = false
    case Recheck if isActive =>
      val sourceFileStatus = readFileStatus(watchedFile.sourceFileName)
      val targetFileStatus = readFileStatus(watchedFile.targetFileName)

      masterActor ! RenderFileStatus(watchedFile, renderFileStatus(sourceFileStatus), renderFileStatus(targetFileStatus))
      masterActor ! RenderFileSyncStatus(watchedFile, "Rechecking", "grey")

      (sourceFileStatus, targetFileStatus) match {
        case (None, None) =>
          masterActor ! createSyncedStatus
        case (None, Some(_)) =>
          masterActor ! RenderFileSyncStatus(watchedFile, "Deleting target file", "grey")
          trySyncOperation {
            Files.delete(Paths.get(watchedFile.targetFileName))
          }
        case (Some(_), None) =>
          masterActor ! RenderFileSyncStatus(watchedFile, "Creating target file", "grey")
          trySyncOperation {
            Files.copy(Paths.get(watchedFile.sourceFileName), Paths.get(watchedFile.targetFileName))
          }
        case (Some(_), Some(_)) =>
          if (sourceFileStatus == targetFileStatus)
            masterActor ! createSyncedStatus
          else {
            masterActor ! RenderFileSyncStatus(watchedFile, "Updating target file", "grey")
            trySyncOperation {
              Files.copy(Paths.get(watchedFile.sourceFileName), Paths.get(watchedFile.targetFileName), StandardCopyOption.REPLACE_EXISTING)
            }
          }
      }
  }

  def trySyncOperation(operation: => Unit): Unit = {
    lastSyncAttempt = Some(new Date)
    println(s"updated last sync to $lastSyncAttempt")
    Try(operation) match {
      case Success(_) =>
        lastSynced = Some(new Date)
        masterActor ! createSyncedStatus
        self ! Recheck
      case Failure(exception) =>
        masterActor ! createSyncFailedStatus(exception)
        Akka.system.scheduler.scheduleOnce(Duration(1, TimeUnit.SECONDS), self, Recheck)
    }
  }

  def createSyncedStatus = RenderFileSyncStatus(
    watchedFile, lastSynced.map(date => s"Synced since ${timestampFormat.format(date)}").getOrElse("Synced"), "green"
  )

  def createSyncFailedStatus(exception: Throwable) = RenderFileSyncStatus(
    watchedFile,
    s"Sync failed: ${exception.getMessage}"
      + lastSynced.map(date => s"Last synced: ${timestampFormat.format(date)}").getOrElse("")
      + lastSyncAttempt.map(date => s"Last sync attempt: ${timestampFormat.format(date)}").getOrElse(""),
    "red"
  )

  def readFileStatus(fileName: String): Option[ExistingFile] = {
    val file = new File(fileName)
    if (file.exists()) {
      val bytes = Files.readAllBytes(Paths.get(fileName))
      Some(ExistingFile(bytes.length, DigestUtils.md5Hex(bytes)))
    }
    else
      None
  }

  def renderFileStatus(status: Option[ExistingFile]) = status match {
    case Some(ExistingFile(size, hash)) => s"Size: $size, hash: $hash"
    case None => "Does not exist"
  }

}
