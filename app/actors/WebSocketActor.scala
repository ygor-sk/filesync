package actors

import akka.actor.{Actor, ActorRef}
import akka.event.LoggingReceive

class WebSocketActor(val webSocketOutput: ActorRef) extends Actor {

  override def preStart(): Unit = {
    fileWatcherActor ! OnWebSocketConnection(this)
  }

  override def receive: Receive = LoggingReceive {
    case message => println(s"WebSocket message: $message")
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    fileWatcherActor ! OnWebSocketDisconnection(this)
  }
}
