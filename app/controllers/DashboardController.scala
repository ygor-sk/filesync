package controllers

import actors.{ReloadConfiguration, WebSocketActor}
import akka.actor.Props
import play.api.Play.current
import play.api.libs.json.JsValue
import play.api.mvc._
import services.DashboardService

class DashboardController extends Controller {

  def index = Action {
    Redirect(routes.DashboardController.dashboard())
  }

  def dashboard = Action {
    val configurationLines = DashboardService.readConfigurationFile
    val watchedFiles = DashboardService.getWatchedFiles(configurationLines)
    Ok(views.html.dashboard(
      watchedFiles,
      configurationLines.mkString("\n")
    ))
  }

  def socket = WebSocket.acceptWithActor[JsValue, JsValue] { request => out =>
    Props(new WebSocketActor(out))
  }

  def saveConfiguration = Action { request =>
    val params: Map[String, Seq[String]] = request.body.asFormUrlEncoded.get
    val configuration = params("configuration").head
    DashboardService.writeConfigurationFile(configuration)
    actors.fileWatcherActor ! ReloadConfiguration
    Redirect(routes.DashboardController.dashboard())
  }

}

