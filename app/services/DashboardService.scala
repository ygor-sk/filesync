package services

import java.io.{File, FileWriter}
import java.security.MessageDigest

import scala.io.Source

object DashboardService {

  case class WatchedFile(sourceFileName: String, targetFileName: String) {
    lazy val uniqueId = MessageDigest.getInstance("MD5").digest((sourceFileName + "->" + targetFileName).getBytes).map("%02x".format(_)).mkString
  }

  def readConfigurationFile: List[String] = {
    val configurationFile = getConfigurationFile
    if (configurationFile.exists())
      Source.fromFile(configurationFile).getLines().toList
    else
      List.empty
  }

  def writeConfigurationFile(configuration: String): Unit = {
    val configurationFile: File = DashboardService.getConfigurationFile
    configurationFile.mkdirs()
    new FileWriter(configurationFile).append(configuration).close()
  }

  def getWatchedFiles(configurationLines: List[String]): List[WatchedFile] = {
    configurationLines
      .map(line => line.split("->"))
      .filter(parts => parts.length == 2)
      .map(parts => new WatchedFile(parts(0).trim, parts(1).trim))
  }

  def getConfigurationFile: File = {
    val userHomeDir = System.getProperty("user.home")
    val appDir = new File(userHomeDir, ".cnc-ws")
    val configurationFile = new File(appDir, "configuration.txt")
    configurationFile
  }

}
